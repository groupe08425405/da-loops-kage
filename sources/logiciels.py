import cgi
print("Content-type: text/html; charset=UTF_8\n")

page="""<!DOCTYPE html>

<html lang="fr">

<meta charset="UTF_8">

<link rel="stylesheet" href="style/logiciels.css">
<link rel="icon" type="image/jpg" href="images/logo.jpg" >
<title>DA LOOPS KAGE - logiciels</title>

<head><h1>Les meilleurs DAW gratuits pour créer de la musique</h1> 

<ul id="nav">
  <li><a href="#flstudio">FL Studio</a></li>
  <li><a href="#ableton">Ableton</a></li>
  <li><a href="#protools">Pro Tools</a></li>
  <li><a href="#renoise">Renoise</a></li>
  <li><a href="#audacity">Audacity</a></li>
</ul>



</head>

<h2 id="flstudio">Fl studio </h2>

<div class="fiche"  > 
	<a href=https://www.image-line.com/fl-studio-download/ target="_blank" title="download FL Studio"> 
	<img class="logo" src="images/logo fl studio.jpg" alt="lien fl studio" /></a> 
	
	    <p> Ce logiciel comprend un éditeur audio, un séquenceur basé 
						  sur le concept de patterns (de type de boîte à rythmes, ou 
						  de type partition) à arranger dans une liste de lecture.
						  FL Studio peut charger et exécuter des plug-ins (instruments virtuels, effets).</p>
	<img class="interface" src="images/screenflstudio.png" alt="interface fl studio"  />
</div>

<h2 id="ableton">Ableton </h2>

<div class="fiche" > <a href=https://www.ableton.com/fr/trial/ target="_blank" title="FL Studio Is Better"> 
	    <img class="logo" src="images/ableton.jpg" alt="lien Ableton Live" /></a> 
	    <p>Ableton Live 11 est un logiciel rapide, fluide et flexible pour la création
							   et la performance musicales. Il est livré avec toutes sortes d'effets, d'instruments,
							   de sons et d'outils créatifs – tout ce dont vous avez besoin pour produire n'importe 
							   quel type de musique.</p>
	<img class="interface" src="images/screenableton.png" alt="interface fl studio"  />
</div>



<h2 id="protools">Pro Tools </h2>

<div class="fiche" > <a href=https://pro-tools-first.fr.softonic.com// target="_blank" title="Download Pro Tools"> 
	   <img class="logo" src="images/logo pro tools.jpg" alt="lien pro tool" /></a> 
	   <p>Pro Tools est une station audio-numérique développée et fabriquée par Avid Technology.
	   Il se retrouve dans des domaines tels que l'enregistrement et le mixage musical, 
	   la post production audio film et télévision, le montage son, le mixage,
	   la création et l'illustration sonore, la création et la composition musicale.   </p>
	<img class="interface" src="images/screenprotools.png" alt="interface fl studio" />
</div>



<h2 id="renoise">Renoise</h2>

<div class="fiche" > <a href=https://www.renoise.com/download target="_blank" title="Download Renoise"> 
	    <img class="logo" src="images/renoise.png" alt="lien studio one 5" /></a> 
	    <p>Renoise est une station de travail audio numérique s'appuyant sur l'héritage et le développement des trackers.
		Il est principalement utilisé dans la composition de musique utilisant des échantillons audio,
		des synthétiseurs virtuels et des effets sonores.</p>
	<img class="interface" src="images/screenrenoise.jpg" alt="interface fl studio" />
</div>



<h2 id="audacity">Audacity</h2>

<div class="fiche" > <a href=https://audacity.fr.uptodown.com/windows/ target="_blank" title="Download Audacity"> 
	    <img class="logo" src="images/logo audacity.jpg" alt="lien audacity" /></a> 
	    <p>Audacity est un logiciel d'enregistrement de son numérique et d'édition de sources audionumériques sous différents formats. 
		Le logiciel est distribué sous licence libre sur Windows, MacOS et Linux.</p>
	<img class="interface" src="images/screenaudacity.jpg" alt="interface fl studio" />
</div>

   
</body>

</html>

"""
print(page)
