
import cgi 
print("Content-type: text/html; charset=UTF_8\n")


page="""<!DOCTYPE html>

<html lang="fr">

<head>

<meta charset="UTF_8">

<link rel="stylesheet" href="style/download.css">
<link rel="icon" type="image/jpg" href="images/logo.jpg" >
<title>DA LOOPS KAGE - Datafile Vol 1</title>

</head>
    <header>
        <h1>Datafile Volume 1</h1> 
    </header>

<body>

<div class="fiche"  > 

	<img class="logo" src="images/ZERO-G_datafile_one.jpg" alt="zero_g_vol_1_image" />
	
	    <p>ZERO G Datafile Vol 1 est une compilation de breaks et de percussion mais aussi d'instruments.
		Publié en 1991 en format physique , ce pack et recompilé et trié pour vous aujourd'hui.
		A base de drums old school, ce kit est parfait pour faire de la Jungle .</p>
	
	<h2>Contenu :</h2>
	<ul>
		<li>62 drum loops</li>
		<li>217 drum one shot</li>
		<li>161 échantillons d'instruments</li>
		<li>344 échantillons vocaux</li>
		<li>et plus encore</li>
	</ul>
	
	<h2>Taille : 619 Mo</h2>
	
    <button><a href="https://drive.google.com/file/d/15LklkvETRAvukfatIFMLm6EhwBx3e9yK/view?usp=drive_link" ><span>Télécharger</span></a></button> 
    
</div>


   
</body></html>
"""


print(page)