
import cgi 
print("Content-type: text/html; charset=UTF_8\n")


page="""<!DOCTYPE html>

<html>

<head>

<meta charset="UTF_8">

<link rel="stylesheet" href="style/download.css">
<link rel="icon" type="image/jpg" href="images/logo.jpg" >
<title>DA LOOPS KAGE - Datafile Vol 1</title>

</head>
    <header>
        <h1><i>200-max</i></h1> 
    </header>

<body>

<div class="fiche"  > 

	<img class="logo" src="images/200max.png"/>
	
	    <p>200 Max est un ensemble de modules Buchla 200.
Cet ensemble d'approximations de modules Buchla 200 est le plus grand nombre 
qui s'affichera confortablement sur la plupart des écrans sans défilement.
 La sélection des modules met l'accent sur la variété des fonctions.

Le module de contrôle du clavier est basé sur certaines des fonctions du 221 Kinesthetic
 Input Port, y compris l'unique "2D Voltage Source", un joystick qui émet quatre tensions 
de contrôle distinctes : du centre vers le haut, du centre vers le bas, du centre vers 
la gauche et du centre vers la droite.

Il ne s'agit PAS d'émulations du son ou de la fonction précise d'un module ou d'un instrument 
Buchla, mais seulement d'un moyen d'expérimenter une partie de la manière unique de la 
"Côte Ouest" de penser la génération et l'altération du son.

L'appariement se fait par des menus déroulants à chaque entrée de signal et de CV.
        </p>
	
    <button><a href="https://drive.google.com/file/d/16vtXDWgztjwK-wjdY8E0-VqbklavJyPU/view?usp=drive_link" ><span>Télécharger</span></a></button> 
    
</div>


   
</body></html>
"""


print(page)