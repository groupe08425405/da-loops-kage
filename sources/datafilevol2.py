
import cgi 
print("Content-type: text/html; charset=UTF_8\n")


page="""<!DOCTYPE html>

<html>
<head>
<meta charset="UTF_8">

<link rel="stylesheet" href="style/download.css">
<link rel="icon" type="image/jpg" href="images/logo.jpg" >
<title>DA LOOPS KAGE - Packs de samples</title>

<head>
<h1>Datafile Volume 2</h1> 
</head>

<div class="fiche"  > 
	 
	<img class="logo" src="images/ZERO-G_datafile_one.jpg" alt="zero_g_vol_1_image" />
	
	    <p>ZERO G Datafile Vol 2 est une compilation de breaks et de percussion mais aussi d'instruments.
		Publié en 1992 en format physique , ce pack et recompilé et trié pour vous aujourd'hui.
		A base de drums old school, ce kit est parfait pour faire de la Jungle .</p>
	
	<h2>Contenu</h2>
	<ul>
		<li>105 drum loops</li>
		<li>181 drum one shot</li>
		<li>111 échantillons d'instruments</li>
		<li>418 échantillons vocaux</li>
		<li>et plus encore</li>
	</ul>
	
	<h2>Taille : 393 Mo</h2>
    
    
    <button><a href="https://drive.google.com/file/d/15UpD2WrOp8zjmpC3YEBzmm6AosdNSiUD/view?usp=drive_link" ><span>Télécharger</span></a></button> 
</div>


</body>

</html>

"""


print(page)