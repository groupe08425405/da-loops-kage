
import cgi 
print("Content-type: text/html; charset=UTF_8\n")


page="""<!DOCTYPE html>

<html>

<head>

<meta charset="UTF_8">

<link rel="stylesheet" href="style/download.css">
<link rel="icon" type="image/jpg" href="images/logo.jpg" >
<title>DA LOOPS KAGE - Datafile Vol 1</title>

</head>
    <header>
        <h1><i>SpaceScraper</i></h1> 
    </header>

<body>

<div class="fiche"  > 

	<img class="logo" src="images/spacescraper.png"  />
	
	    <p>SpaceScraper est basé sur un nouvel algorithme de distorsion adaptatif qui offre une gamme de sons innovante et étendue.
 Du sifflement strident au bouillonnement doux, tout est possible.

Le SpaceScraper intègre une architecture interne complexe dans une interface très conviviale. 
Sélectionnez l'un des sept modes disponibles pour obtenir une impression sonore complètement différente.
 Essayez l'influence des paramètres Rise et Fall et du filtre passe-haut analogique. C'est magique !

        </p>
	
    <button><a href="https://drive.google.com/file/d/1lkKAc1stXlY6UuDstSPCuvabjwuBg1I0/view?usp=drive_link" ><span>Télécharger</span></a></button> 
    
</div>


   
</body></html>
"""


print(page)